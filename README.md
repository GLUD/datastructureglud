# DataStructureGLUD

Estructura de Base de Datos para el Grupo GNU Linux UD - GLUD

Objetivo: Mantener la persistencia de los datos del Grupo.

## Documentación de montaje

Todo el proceso realizado para montar la Base de datos PostgreSQL en un contenedor Docker en el servidor del Grupo GNU Linux Universidad Distrital está documentado en:

https://glud.udistrital.edu.co/contenedor-docker-para-postgresql/
