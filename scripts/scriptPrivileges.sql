/*  Grupo GNU Linux UD - 2020 
    LeidyMarcelaAldana@gmail.com
    chayderarmando@gmail.com
*/

/*FIFTH SCRIPT!!!!*/

SET search_path TO generalSchema, eventosSchema, inventariosSchema, finanzasSchema, proyectosSchema, "$user", public;
/*
Assign Privileges over DB to roles
*/
GRANT CONNECT ON DATABASE db_glud TO Director;
GRANT CONNECT ON DATABASE db_glud TO Tesorero;
GRANT CONNECT ON DATABASE db_glud TO AdminSala;
GRANT CONNECT ON DATABASE db_glud TO Miembro;
GRANT CONNECT ON DATABASE db_glud TO Invitado;
/*
Assign Privileges over Schemas to roles
*/
--Director role
GRANT USAGE ON SCHEMA generalSchema TO Director;
GRANT USAGE ON SCHEMA proyectosSchema TO Director;
GRANT USAGE ON SCHEMA eventosSchema TO Director;
GRANT USAGE ON SCHEMA inventariosSchema TO Director;
GRANT USAGE ON SCHEMA finanzasSchema TO Director;

--Tesorero role
GRANT USAGE ON SCHEMA generalSchema TO Tesorero;
GRANT USAGE ON SCHEMA proyectosSchema TO Tesorero;
GRANT USAGE ON SCHEMA eventosSchema TO Tesorero;
GRANT USAGE ON SCHEMA inventariosSchema TO Tesorero;
GRANT USAGE ON SCHEMA finanzasSchema TO Tesorero;

--AdminSala role
GRANT USAGE ON SCHEMA generalSchema TO AdminSala;
GRANT USAGE ON SCHEMA proyectosSchema TO AdminSala;
GRANT USAGE ON SCHEMA eventosSchema TO AdminSala;
GRANT USAGE ON SCHEMA inventariosSchema TO AdminSala;
GRANT USAGE ON SCHEMA finanzasSchema TO AdminSala;

--Miembro role
GRANT USAGE ON SCHEMA generalSchema TO Miembro;
GRANT USAGE ON SCHEMA proyectosSchema TO Miembro;
GRANT USAGE ON SCHEMA eventosSchema TO Miembro;
GRANT USAGE ON SCHEMA inventariosSchema TO Miembro;
GRANT USAGE ON SCHEMA finanzasSchema TO Miembro;

--Invitado role
GRANT USAGE ON SCHEMA generalSchema TO Invitado;
GRANT USAGE ON SCHEMA proyectosSchema TO Invitado;
GRANT USAGE ON SCHEMA eventosSchema TO Invitado;
GRANT USAGE ON SCHEMA inventariosSchema TO Invitado;


/*
Assign Privileges over tables to roles
*/

/*---------------------*/
/*---------------------*/
/* DIRECTOR PRIVILEGES */
/*---------------------*/
/*---------------------*/

/*---------------------*/
/*SistemaGeneral Schema*/
/*---------------------*/
--SET search_path TO generalSchema;
GRANT SELECT, INSERT ON AnioGlud TO Director;
GRANT SELECT, INSERT, UPDATE ON Persona TO Director;
GRANT SELECT, INSERT, UPDATE ON Miembro TO Director;
GRANT SELECT ON Cargo TO Director;
GRANT SELECT, INSERT, UPDATE ON CargoEjercido TO Director;
GRANT SELECT, INSERT ON ProyectoCurricular TO Director;

/*-----------------------*/
/*SistemaProyectos Schema*/
/*-----------------------*/
--SET search_path TO proyectosSchema;
GRANT SELECT, INSERT, UPDATE ON Proyecto TO Director;
GRANT SELECT, INSERT, UPDATE ON MiembroProyecto TO Director;

/*---------------------*/
/*SistemaEventos Schema*/
/*---------------------*/
--SET search_path TO eventosSchema;
GRANT SELECT, INSERT, UPDATE ON Evento TO Director;
GRANT SELECT, INSERT, UPDATE, DELETE ON Actividad TO Director;
GRANT SELECT, INSERT, UPDATE ON Patrocinador TO Director;
GRANT SELECT, INSERT, UPDATE ON Financiacion TO Director;
GRANT SELECT ON Inscripcion TO Director;

/*--------------------------*/
/* SistemaInventario Schema */
/*--------------------------*/
--SET search_path TO inventariosSchema;
GRANT SELECT ON AdministracionInventario TO Director;
GRANT SELECT, INSERT, UPDATE ON Articulo TO Director;
GRANT SELECT, INSERT ON ArticuloInventario TO Director;
GRANT SELECT, INSERT, UPDATE ON Libro TO Director;
GRANT SELECT, INSERT, UPDATE ON ArticuloElectronico TO Director;
GRANT SELECT, INSERT, UPDATE ON Miscelanea TO Director;
GRANT SELECT, INSERT, UPDATE ON Juego TO Director;
GRANT SELECT, INSERT, UPDATE ON Publicidad TO Director;
GRANT SELECT, INSERT, UPDATE ON Otro TO Director;
GRANT SELECT, INSERT, UPDATE ON Prestamo TO Director;

/*--------------------------*/
/* SistemaFinanciero Schema */
/*--------------------------*/
--SET search_path TO finanzasSchema;
GRANT SELECT ON AdministracionTesoreria TO Director;
GRANT SELECT ON Movimiento TO Director;


/*---------------------*/
/*---------------------*/
/* TESORERO PRIVILEGES */
/*---------------------*/
/*---------------------*/

/*---------------------*/
/*SistemaGeneral Schema*/
/*---------------------*/
--SET search_path TO generalSchema;
GRANT SELECT ON AnioGlud TO Tesorero;
GRANT SELECT, INSERT, UPDATE ON Persona TO Tesorero;
GRANT SELECT, INSERT, UPDATE ON Miembro TO Tesorero;
GRANT SELECT ON Cargo TO Tesorero;
GRANT SELECT ON CargoEjercido TO Tesorero;
GRANT SELECT ON ProyectoCurricular TO Tesorero;

/*-----------------------*/
/*SistemaProyectos Schema*/
/*-----------------------*/
--SET search_path TO proyectosSchema;
GRANT SELECT, INSERT, UPDATE ON Proyecto TO Tesorero;
GRANT SELECT, INSERT, UPDATE ON MiembroProyecto TO Tesorero;

/*---------------------*/
/*SistemaEventos Schema*/
/*---------------------*/
--SET search_path TO eventosSchema;
GRANT SELECT ON Evento TO Tesorero;
GRANT SELECT, INSERT, UPDATE ON Actividad TO Tesorero;
GRANT SELECT, INSERT, UPDATE ON Patrocinador TO Tesorero;
GRANT SELECT, INSERT, UPDATE, DELETE ON Financiacion TO Tesorero;
GRANT SELECT ON Inscripcion TO Tesorero;

/*--------------------------*/
/* SistemaInventario Schema */
/*--------------------------*/
--SET search_path TO inventariosSchema;
GRANT SELECT ON AdministracionInventario TO Tesorero;
GRANT SELECT, INSERT, UPDATE ON Articulo TO Tesorero;
GRANT SELECT, INSERT ON ArticuloInventario TO Tesorero;
GRANT SELECT, INSERT, UPDATE ON Libro TO Tesorero;
GRANT SELECT, INSERT, UPDATE ON ArticuloElectronico TO Tesorero;
GRANT SELECT, INSERT, UPDATE ON Miscelanea TO Tesorero;
GRANT SELECT, INSERT, UPDATE ON Juego TO Tesorero;
GRANT SELECT, INSERT, UPDATE ON Publicidad TO Tesorero;
GRANT SELECT, INSERT, UPDATE ON Otro TO Tesorero;
GRANT SELECT, INSERT, UPDATE ON Prestamo TO Tesorero;

/*--------------------------*/
/* SistemaFinanciero Schema */
/*--------------------------*/
--SET search_path TO finanzasSchema;
GRANT SELECT, INSERT ON AdministracionTesoreria TO Tesorero;
GRANT SELECT, INSERT, UPDATE ON Movimiento TO Tesorero;


/*----------------------*/
/*----------------------*/
/* ADMINSALA PRIVILEGES */
/*----------------------*/
/*----------------------*/

/*---------------------*/
/*SistemaGeneral Schema*/
/*---------------------*/
--SET search_path TO generalSchema;
GRANT SELECT ON AnioGlud TO AdminSala;
GRANT SELECT, INSERT, UPDATE ON Persona TO AdminSala;
GRANT SELECT, INSERT, UPDATE ON Miembro TO AdminSala;
GRANT SELECT ON Cargo TO AdminSala;
GRANT SELECT ON CargoEjercido TO AdminSala;
GRANT SELECT ON ProyectoCurricular TO AdminSala;

/*-----------------------*/
/*SistemaProyectos Schema*/
/*-----------------------*/
--SET search_path TO proyectosSchema;
GRANT SELECT, INSERT, UPDATE ON Proyecto TO AdminSala;
GRANT SELECT, INSERT, UPDATE ON MiembroProyecto TO AdminSala;

/*-----------------------*/
/* SistemaEventos Schema */
/*-----------------------*/
--SET search_path TO eventosSchema;
GRANT SELECT ON Evento TO AdminSala;
GRANT SELECT, INSERT, UPDATE ON Actividad TO AdminSala;
GRANT SELECT ON Patrocinador TO AdminSala;
GRANT SELECT ON Financiacion TO AdminSala;
GRANT SELECT ON Inscripcion TO AdminSala;

/*--------------------------*/
/* SistemaInventario Schema */
/*--------------------------*/
--SET search_path TO inventariosSchema;
GRANT SELECT, INSERT ON AdministracionInventario TO AdminSala;
GRANT SELECT, INSERT, UPDATE ON Articulo TO AdminSala;
GRANT SELECT, INSERT, UPDATE, DELETE ON ArticuloInventario TO AdminSala;
GRANT SELECT, INSERT, UPDATE ON Libro TO AdminSala;
GRANT SELECT, INSERT, UPDATE ON ArticuloElectronico TO AdminSala;
GRANT SELECT, INSERT, UPDATE ON Miscelanea TO AdminSala;
GRANT SELECT, INSERT, UPDATE ON Juego TO AdminSala;
GRANT SELECT, INSERT, UPDATE ON Publicidad TO AdminSala;
GRANT SELECT, INSERT, UPDATE ON Otro TO AdminSala;
GRANT SELECT, INSERT, UPDATE ON Prestamo TO AdminSala;

/*--------------------------*/
/* SistemaFinanciero Schema */
/*--------------------------*/
--SET search_path TO finanzasSchema;
GRANT SELECT ON AdministracionTesoreria TO AdminSala;
GRANT SELECT ON Movimiento TO AdminSala;


/*---------------------*/
/*---------------------*/
/* MIEMBRO PRIVILEGES  */
/*---------------------*/
/*---------------------*/

/*---------------------*/
/*SistemaGeneral Schema*/
/*---------------------*/
--SET search_path TO generalSchema;
GRANT SELECT ON AnioGlud TO Miembro;
GRANT SELECT, INSERT, UPDATE ON Persona TO Miembro;
GRANT SELECT, INSERT, UPDATE ON Miembro TO Miembro;
GRANT SELECT ON Cargo TO Miembro;
GRANT SELECT ON CargoEjercido TO Miembro;
GRANT SELECT ON ProyectoCurricular TO Miembro;

/*-----------------------*/
/*SistemaProyectos Schema*/
/*-----------------------*/
--SET search_path TO proyectosSchema;
GRANT SELECT, INSERT, UPDATE ON Proyecto TO Miembro;
GRANT SELECT, INSERT, UPDATE ON MiembroProyecto TO Miembro;

/*---------------------*/
/*SistemaEventos Schema*/
/*---------------------*/
--SET search_path TO eventosSchema;
GRANT SELECT ON Evento TO Miembro;
GRANT SELECT, INSERT, UPDATE ON Actividad TO Miembro;
GRANT SELECT ON Patrocinador TO Miembro;
GRANT SELECT ON Financiacion TO Miembro;
GRANT SELECT ON Inscripcion TO Miembro;

/*--------------------------*/
/* SistemaInventario Schema */
/*--------------------------*/
--SET search_path TO inventariosSchema;
GRANT SELECT ON AdministracionInventario TO Miembro;
GRANT SELECT ON Articulo TO Miembro;
GRANT SELECT ON ArticuloInventario TO Miembro;
GRANT SELECT ON Libro TO Miembro;
GRANT SELECT ON ArticuloElectronico TO Miembro;
GRANT SELECT ON Miscelanea TO Miembro;
GRANT SELECT ON Juego TO Miembro;
GRANT SELECT ON Publicidad TO Miembro;
GRANT SELECT ON Otro TO Miembro;
GRANT SELECT, INSERT, UPDATE ON Prestamo TO Miembro;

/*--------------------------*/
/* SistemaFinanciero Schema */
/*--------------------------*/
--SET search_path TO finanzasSchema;
GRANT SELECT ON AdministracionTesoreria TO Miembro;
GRANT SELECT ON Movimiento TO Miembro;


/*-----------------------*/
/*-----------------------*/
/*  INVITADO PRIVILEGES  */
/*-----------------------*/
/*----------------------.*/

/*---------------------*/
/*SistemaGeneral Schema*/
/*---------------------*/
--SET search_path TO generalSchema;
GRANT INSERT, UPDATE ON Persona TO Invitado;
GRANT SELECT ON Miembro TO Invitado;
GRANT SELECT ON CargoEjercido TO Invitado;

/*-----------------------*/
/*SistemaProyectos Schema*/
/*-----------------------*/
--SET search_path TO proyectosSchema;
GRANT SELECT ON Proyecto TO Invitado;
GRANT SELECT ON MiembroProyecto TO Invitado;

/*---------------------*/
/*SistemaEventos Schema*/
/*---------------------*/
--SET search_path TO eventosSchema;
GRANT SELECT ON Evento TO Invitado;
GRANT SELECT ON Actividad TO Invitado;
GRANT SELECT, INSERT ON Patrocinador TO Invitado;
GRANT INSERT ON Inscripcion TO Invitado;

/*--------------------------*/
/* SistemaInventario Schema */
/*--------------------------*/
--SET search_path TO inventariosSchema;
GRANT SELECT ON AdministracionInventario TO Invitado;
GRANT SELECT ON Articulo TO Invitado;
GRANT SELECT ON ArticuloInventario TO Invitado;
GRANT SELECT ON Libro TO Invitado;
GRANT SELECT ON ArticuloElectronico TO Invitado;
GRANT SELECT ON Miscelanea TO Invitado;
GRANT SELECT ON Juego TO Invitado;
GRANT SELECT ON Publicidad TO Invitado;
GRANT SELECT ON Otro TO Invitado;

/*--------------------------*/
/* SistemaFinanciero Schema */
/*--------------------------*/
/* Guest haven't privileges in this Schema */
/*
--SET search_path TO finanzasSchema;
GRANT SELECT ON AdministracionTesoreria TO Invitado;
GRANT SELECT ON Movimiento TO Invitado;
*/


/*
Assign default search_paths to roles
*/
ALTER ROLE dbadmin SET search_path TO generalSchema, eventosSchema, inventariosSchema, finanzasSchema, proyectosSchema, public;
ALTER ROLE director SET search_path TO public, generalSchema, eventosSchema, inventariosSchema, finanzasSchema, proyectosSchema;
ALTER ROLE tesorero SET search_path TO public, generalSchema, eventosSchema, inventariosSchema, finanzasSchema, proyectosSchema;
ALTER ROLE adminsala SET search_path TO public, generalSchema, eventosSchema, inventariosSchema, finanzasSchema, proyectosSchema;
ALTER ROLE miembro SET search_path TO public, generalSchema, eventosSchema, inventariosSchema, finanzasSchema, proyectosSchema;
ALTER ROLE invitado SET search_path TO public, generalSchema, eventosSchema, inventariosSchema, proyectosSchema;