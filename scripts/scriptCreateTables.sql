/*  Grupo GNU Linux UD - 2020 
    LeidyMarcelaAldana@gmail.com
    chayderarmando@gmail.com
*/

/*THIRD SCRIPT!!!*/

/* ----------------------------------------------------- */
--Creacion de tabla de auditoria
/* ----------------------------------------------------- */
/*
DROP TABLE IF EXISTS generalSchema.Auditoria CASCADE ;
-- timestamp with time zone COT
CREATE TABLE generalSchema.Auditoria (
  "k_idaudito" int PRIMARY KEY,
  "c_campoactualizado" varchar(30),
  "d_modificacion" datetime DEFAULT (now()),
  "c_usuario" varchar(20),
  "c_datoanterior" varchar(200),
  "c_datonuevo" varchar(200)
) TABLESPACE sistemaGeneral;

COMMENT ON TABLE Auditoria IS 'Auditoria realizada al interior del Glud';
COMMENT ON COLUMN generalSchema.Auditoria.k_idaudito IS 'Llave primaria';
COMMENT ON COLUMN generalSchema.Auditoria.c_campoactualizado IS 'Campo que actualiza un usuario';
COMMENT ON COLUMN generalSchema.Auditoria.d_modificacion IS 'Fecha en la cual se modifica ese campo';
COMMENT ON COLUMN generalSchema.Auditoria.c_usuario IS 'Usuario que modifica el campo';
COMMENT ON COLUMN generalSchema.Auditoria.c_datoanterior IS 'Dato que estaba antes en la base de datos';
COMMENT ON COLUMN generalSchema.Auditoria.c_datonuevo IS 'Nuevo dato que esta siendo aniadido';
*/

/* ----------------------------------------------------- */
--Creacion de tablas
/* ----------------------------------------------------- */ 
DROP TABLE IF EXISTS generalSchema.AnioGlud CASCADE ;

CREATE TABLE generalSchema.AnioGlud (
  "k_id" int PRIMARY KEY
) TABLESPACE sistemaGeneral;

COMMENT ON TABLE generalSchema.AnioGlud IS 'Registro de los años vigentes en el Glud';
COMMENT ON COLUMN generalSchema.AnioGlud.k_id IS 'Llave primaria';

ALTER TABLE generalSchema.AnioGlud ADD CONSTRAINT CHK_IDGLUD CHECK (k_id>0);

-- INT PARA IDENTIFICACION PUEDE QUEDARSE CORTO (10 DIGITOS)
DROP TABLE IF EXISTS generalSchema.Persona CASCADE ;

CREATE TABLE generalSchema.Persona (
  "k_tipoid" varchar(2), 
  "k_id" int,
  "c_nombre" varchar(20) NOT NULL,
  "c_apellido" varchar(20) NOT NULL,
  "c_correo" varchar(40) NOT NULL,
  "c_telefono" varchar(10),
  PRIMARY KEY ("k_tipoid", "k_id")
) TABLESPACE sistemaGeneral;

COMMENT ON TABLE generalSchema.Persona IS 'Personas relacionadas con el Glud';
COMMENT ON COLUMN generalSchema.Persona.k_tipoid IS 'Llave primaria, tipo de documento de identidad';
COMMENT ON COLUMN generalSchema.Persona.k_id IS 'Llave primaria, documento de identidad';
COMMENT ON COLUMN generalSchema.Persona.c_nombre IS 'Ambos nombres de la persona';
COMMENT ON COLUMN generalSchema.Persona.c_apellido IS 'Ambos apellidos de la persona';
COMMENT ON COLUMN generalSchema.Persona.c_correo IS 'Correo electronico';
COMMENT ON COLUMN generalSchema.Persona.c_telefono IS 'Telefono de contacto';

ALTER TABLE generalSchema.Persona ADD CONSTRAINT CHK_TIPOIDPERSONA CHECK (k_tipoid IN ('TI', 'CC', 'CE'));
ALTER TABLE generalSchema.Persona ADD CONSTRAINT CHK_IDPERSONA CHECK (k_id>0);
ALTER TABLE generalSchema.Persona ADD CONSTRAINT CHK_CORREO CHECK (c_correo LIKE '%@%');

DROP TABLE IF EXISTS generalSchema.Miembro CASCADE ;

CREATE TABLE generalSchema.Miembro (
  "k_tipoid" varchar(2),
  "k_id" int,
  "n_codigo" bigint UNIQUE NOT NULL,
  "k_tipoiddocente" varchar(2),
  "k_iddocente" int,
  "d_fechaingreso" date NOT NULL,
  "d_fechaegreso" date,
  "k_idanioglud" int,
  "c_estado" varchar(20) NOT NULL,
  "c_tallacamisa" varchar(2),
  "c_observaciones" varchar(100),
  "c_genero" varchar(1) NOT NULL,
  "k_idproyectocurricular" int NOT NULL,
  "c_ocupacion" varchar(50),
  "c_username" varchar(20) UNIQUE NOT NULL,
  PRIMARY KEY ("k_tipoid", "k_id")
) TABLESPACE	sistemaGeneral ;

COMMENT ON TABLE generalSchema.Miembro IS 'Tabla que almacena cada uno de los miembros que han pasado por el Grupo GNU Linux Universidad Distrital FJC';
COMMENT ON COLUMN generalSchema.Miembro.k_tipoid IS 'Llave primaria, tipo de documento de identificacion enlazado como llave foranea';
COMMENT ON COLUMN generalSchema.Miembro.k_id IS 'Llave primaria, documento de identificacion enlazado como llave foranea';
COMMENT ON COLUMN generalSchema.Miembro.n_codigo IS 'Codigo estudiantil';
COMMENT ON COLUMN generalSchema.Miembro.k_tipoiddocente IS 'Llave foranea del tipo de identificacion del docente con el cual tomo el curso de GNUbies';
COMMENT ON COLUMN generalSchema.Miembro.k_iddocente IS 'Llave foranea de la identificacion del docente con el cual tomo el curso de GNUbies';
COMMENT ON COLUMN generalSchema.Miembro.d_fechaingreso IS 'Fecha en la cual el miembro activo ingreso al grupo';
COMMENT ON COLUMN generalSchema.Miembro.d_fechaegreso IS 'Fecha en la cual dejo de ser miembro activo del Glud';
COMMENT ON COLUMN generalSchema.Miembro.k_idanioglud IS 'Llave foranea de Anio en el cual fue creado el registro';
COMMENT ON COLUMN generalSchema.Miembro.c_estado IS 'Estado puede variar entre miembro activo, ex miembro, miembro honorario, GNUbie, docente coordinador';
COMMENT ON COLUMN generalSchema.Miembro.c_tallacamisa IS 'Talla de la camisa para posibles eventos';
COMMENT ON COLUMN generalSchema.Miembro.c_observaciones IS 'Observaciones adicionales que deban conocerse';
COMMENT ON COLUMN generalSchema.Miembro.c_genero IS 'Varia entre F o M';
COMMENT ON COLUMN generalSchema.Miembro.k_idproyectocurricular IS 'Llave foranea del proyecto curricular al cual pertenece';
COMMENT ON COLUMN generalSchema.Miembro.c_ocupacion IS 'Posible ocupacion si existe';
COMMENT ON COLUMN generalSchema.Miembro.c_username IS 'Usuario para ingreso';

ALTER TABLE generalSchema.Miembro ADD CONSTRAINT CHK_GENEROM CHECK (c_genero IN ('F','M'));
ALTER TABLE generalSchema.Miembro ADD CONSTRAINT CHK_CODIGOM CHECK (n_codigo>9999999999);
ALTER TABLE generalSchema.Miembro ADD CONSTRAINT CHK_ESTADOM CHECK (c_estado IN ('MIEMBRO ACTIVO', 'EX MIEMBRO', 'MIEMBRO HONORARIO', 'GNUBIE', 'DOCENTE COORDINADOR'));

DROP TABLE IF EXISTS generalSchema.ProyectoCurricular CASCADE ;

CREATE TABLE generalSchema.ProyectoCurricular (
  "k_id" SERIAL PRIMARY KEY,
  "c_nombre" varchar(30) NOT NULL
) TABLESPACE sistemaGeneral;

COMMENT ON TABLE generalSchema.ProyectoCurricular IS 'Proyectos Curriculares a los cuales pertenecen los miembros del Glud';
COMMENT ON COLUMN generalSchema.ProyectoCurricular.k_id IS 'Llave primaria';
COMMENT ON COLUMN generalSchema.ProyectoCurricular.c_nombre IS 'Nombre del proyecto curricular al interior de la Universidad';

DROP TABLE IF EXISTS generalSchema.Cargo CASCADE ;

CREATE TABLE generalSchema.Cargo (
  "k_id" SERIAL PRIMARY KEY,
  "c_nombre" varchar(30) NOT NULL,
  "c_descripcion" varchar(200) NOT NULL
) TABLESPACE sistemaGeneral;

COMMENT ON TABLE generalSchema.Cargo IS 'Cargos al interior del Glud ejercidos por los miembros del Grupo GLUD';
COMMENT ON COLUMN generalSchema.Cargo.k_id IS 'Llave primaria';
COMMENT ON COLUMN generalSchema.Cargo.c_nombre IS 'Nombre del cargo';
COMMENT ON COLUMN generalSchema.Cargo.c_descripcion IS 'Descripcion del cargo';


DROP TABLE IF EXISTS generalSchema.CargoEjercido CASCADE ;

CREATE TABLE generalSchema.CargoEjercido (
  "k_tipoidmiembro" varchar(2), 
  "k_idmiembro" int,
  "k_idcargo" int,
  "d_fechainicio" date NOT NULL,
  "d_fechafin" date,
  PRIMARY KEY ("k_tipoidmiembro", "k_idmiembro", "k_idcargo")
) TABLESPACE sistemaGeneral;

COMMENT ON TABLE generalSchema.CargoEjercido IS 'Cargo ejercido por un miembro al interior del Glud';
COMMENT ON COLUMN generalSchema.CargoEjercido.k_tipoidmiembro IS 'Llave foranea del tipo de identificacion del miembro activo';
COMMENT ON COLUMN generalSchema.CargoEjercido.k_idmiembro IS 'Llave foranea de la identificacion del miembro activo';
COMMENT ON COLUMN generalSchema.CargoEjercido.k_idcargo IS 'Llave foranea del cargo en cuestion';
COMMENT ON COLUMN generalSchema.CargoEjercido.d_fechainicio IS 'Fecha en la cual un miembro activo empieza a hacerse responsable de cierto cargo';
COMMENT ON COLUMN generalSchema.CargoEjercido.d_fechafin IS 'Fecha hasta la cual ese miembro activo desempenia determinado cargo';

--ALTER TABLE generalSchema.CargoEjercido ADD CONSTRAINT CHK_IDCARGOCE CHECK (d_fechafin>d_fechainicio);

DROP TABLE IF EXISTS proyectosSchema.Proyecto CASCADE ;

CREATE TABLE proyectosSchema.Proyecto (
  "k_id" SERIAL PRIMARY KEY,
  "c_nombre" varchar(30) NOT NULL,
  "c_descripcion" varchar(200),
  "c_tipo" varchar(20),
  "c_anteproyecto" varchar(200),
  "c_estado" varchar(16),  
  "c_observaciones" varchar(200)
) TABLESPACE	sistemaProyectos ;

COMMENT ON TABLE proyectosSchema.Proyecto IS 'Proyectos realizados por los Miembros del GLUD';
COMMENT ON COLUMN proyectosSchema.Proyecto.k_id IS 'Llave primaria del proyecto';
COMMENT ON COLUMN proyectosSchema.Proyecto.c_nombre IS 'Nombre del proyecto';
COMMENT ON COLUMN proyectosSchema.Proyecto.c_descripcion IS 'Descripcion del proyecto';
COMMENT ON COLUMN proyectosSchema.Proyecto.c_tipo IS 'Especifica si es de Investigacion, Apropiacion, Desarrollo, Traduccion, o Capacitacion';
COMMENT ON COLUMN proyectosSchema.Proyecto.c_anteproyecto IS 'Ruta donde esta almacenado el anteproyecto';
COMMENT ON COLUMN proyectosSchema.Proyecto.c_estado IS 'Estado del proyecto, varia entre propuesto, aprobado, terminado, incompleto';
COMMENT ON COLUMN proyectosSchema.Proyecto.c_observaciones IS 'Observaciones adicionales del proyecto';

ALTER TABLE proyectosSchema.Proyecto ADD CONSTRAINT CHK_TIPOP CHECK (c_tipo IN ('INVESTIGACION', 'APROPIACION', 'DESARROLLO', 'TRADUCCION', 'CAPACITACION'));
ALTER TABLE proyectosSchema.Proyecto ADD CONSTRAINT CHK_ESTADOP CHECK (c_estado IN ('PROPUESTO','APROBADO','TERMINADO','INCOMPLETO'));

DROP TABLE IF EXISTS proyectosSchema.MiembroProyecto CASCADE ;

CREATE TABLE proyectosSchema.MiembroProyecto (
  "k_tipoidmiembro" varchar(2),
  "k_idmiembro" int,
  "k_idproyecto" int,
  "c_semestre" varchar(5),
  "d_fechainicio" date,
  "d_fechafin" date,
  PRIMARY KEY ("k_tipoidmiembro", "k_idmiembro", "k_idproyecto")
) TABLESPACE sistemaProyectos;

COMMENT ON TABLE proyectosSchema.MiembroProyecto IS 'Tabla de rompiemiento entre proyectos y miembros al interior del Glud';
COMMENT ON COLUMN proyectosSchema.MiembroProyecto.k_idmiembro IS 'Llave PFK, tipo de identificacion del miembro activo';
COMMENT ON COLUMN proyectosSchema.MiembroProyecto.k_idmiembro IS 'Llave PFK, identificacion del miembro activo';
COMMENT ON COLUMN proyectosSchema.MiembroProyecto.k_idproyecto IS 'Llave PFK, id del proyecto para ese miembro activo';
COMMENT ON COLUMN proyectosSchema.MiembroProyecto.c_semestre IS 'Semestre en el cual es desarrollado el proyecto en formato AAAAS por ejemplo 20201';
COMMENT ON COLUMN proyectosSchema.MiembroProyecto.d_fechainicio IS 'Fecha de inicio del proyecto';
COMMENT ON COLUMN proyectosSchema.MiembroProyecto.d_fechafin IS 'Fecha en la que termina el proyecto';

DROP TABLE IF EXISTS inventariosSchema.AdministracionInventario CASCADE ;

CREATE TABLE inventariosSchema.AdministracionInventario (
  "k_id" SERIAL PRIMARY KEY,
  "k_fechavigencia" int NOT NULL,
  "k_tipoidmiembro" varchar(2) NOT NULL,
  "k_idmiembro" int NOT NULL,
  "k_idcargo" int NOT NULL
) TABLESPACE sistemaInventarios;

COMMENT ON TABLE inventariosSchema.AdministracionInventario IS 'Administracion del Inventario al interior del Glud';
COMMENT ON COLUMN inventariosSchema.AdministracionInventario.k_id IS 'Llave primaria';
COMMENT ON COLUMN inventariosSchema.AdministracionInventario.k_fechavigencia IS 'Llave foranea de la fecha en la cual esta vigente el inventario';
COMMENT ON COLUMN inventariosSchema.AdministracionInventario.k_tipoidmiembro IS 'Llave foranea compuesta del tipo de identiicacion del miembro activo que administra el inventario';
COMMENT ON COLUMN inventariosSchema.AdministracionInventario.k_idmiembro IS 'Llave foranea compuesta del miembro activo que administra el inventario';
COMMENT ON COLUMN inventariosSchema.AdministracionInventario.k_idcargo IS 'Llave foranea compuesta del cargo desempeñado por dicho miembro activo al interior del grupo';

DROP TABLE IF EXISTS inventariosSchema.Articulo CASCADE ;

CREATE TABLE inventariosSchema.Articulo (
  "k_id" SERIAL PRIMARY KEY,
  "c_nombre" varchar(30) NOT NULL,
  "c_descripcion" varchar(200),
  "c_ubicacion" varchar(30),
  "c_estadoarticulo" varchar(30),  
  "c_estadodisponible" varchar(1) NOT NULL,
  "n_cantidad" int,
  "c_observaciones" varchar(200)
) TABLESPACE sistemaInventarios;

COMMENT ON TABLE inventariosSchema.Articulo IS 'Articulos que le pertenecen al Glud';
COMMENT ON COLUMN inventariosSchema.Articulo.k_id IS 'Llave primaria';
COMMENT ON COLUMN inventariosSchema.Articulo.c_nombre IS 'Nombre del articulo';
COMMENT ON COLUMN inventariosSchema.Articulo.c_descripcion IS 'Descripcion del articulo';
COMMENT ON COLUMN inventariosSchema.Articulo.c_ubicacion IS 'Ubicacion del articulo';
COMMENT ON COLUMN inventariosSchema.Articulo.c_estadoarticulo IS 'Estado actual del articulo, por ejemplo piezas faltantes';
COMMENT ON COLUMN inventariosSchema.Articulo.c_estadodisponible IS 'Estado para saber si el articulo esta disponible, prestado o perdido';
COMMENT ON COLUMN inventariosSchema.Articulo.n_cantidad IS 'Cantidad de ese articulo con el que cuenta el Glud';
COMMENT ON COLUMN inventariosSchema.Articulo.c_observaciones IS 'Observaciones adicionales del articulo';

ALTER TABLE inventariosSchema.Articulo ADD CONSTRAINT CHK_ARTDISPONIBLE CHECK (c_estadodisponible IN ('DISPONIBLE','PRESTADO','PERDIDO'));
ALTER TABLE inventariosSchema.Articulo ADD CONSTRAINT CHK_ARTCANTIDAD CHECK (n_cantidad>=0);

DROP TABLE IF EXISTS inventariosSchema.Libro CASCADE ;

CREATE TABLE inventariosSchema.Libro (
  "k_idarticulo" int PRIMARY KEY,
  "c_autor" varchar(30),
  "c_editorial" varchar(30)
) TABLESPACE sistemaInventarios;

COMMENT ON TABLE inventariosSchema.Libro IS 'Libros que pertenecen a la biblioteca del Glud';
COMMENT ON COLUMN inventariosSchema.Libro.k_idarticulo IS 'Llave primaria y foranea del articulo';
COMMENT ON COLUMN inventariosSchema.Libro.c_autor IS 'Autor del libro';
COMMENT ON COLUMN inventariosSchema.Libro.c_editorial IS 'Editorial del libro';

DROP TABLE IF EXISTS inventariosSchema.ArticuloElectronico CASCADE ;

CREATE TABLE inventariosSchema.ArticuloElectronico (
  "k_idarticulo" int PRIMARY KEY,
  "c_marca" varchar(20)
) TABLESPACE sistemaInventarios;

COMMENT ON TABLE inventariosSchema.ArticuloElectronico IS 'Articulos Electronicos que pertenecen al Glud';
COMMENT ON COLUMN inventariosSchema.ArticuloElectronico.k_idarticulo IS 'Llave primaria y foranea del articulo';
COMMENT ON COLUMN inventariosSchema.ArticuloElectronico.c_marca IS 'Marca del articulo';

DROP TABLE IF EXISTS inventariosSchema.Miscelanea CASCADE ;

CREATE TABLE inventariosSchema.Miscelanea (
  "k_idarticulo" int PRIMARY KEY
) TABLESPACE sistemaInventarios;

COMMENT ON TABLE inventariosSchema.Miscelanea IS 'Articulos Electronicos que pertenecen al Glud';
COMMENT ON COLUMN inventariosSchema.Miscelanea.k_idarticulo IS 'Llave primaria y foranea del articulo';

DROP TABLE IF EXISTS inventariosSchema.Juego CASCADE ;

CREATE TABLE inventariosSchema.Juego (
  "k_idarticulo" int PRIMARY KEY
) TABLESPACE sistemaInventarios;

COMMENT ON TABLE inventariosSchema.Juego IS 'Juegos de mesa que pertenecen al Glud';
COMMENT ON COLUMN inventariosSchema.Juego.k_idarticulo IS 'Llave primaria y foranea del articulo';

DROP TABLE IF EXISTS inventariosSchema.Publicidad CASCADE ;

CREATE TABLE inventariosSchema.Publicidad (
  "k_idarticulo" int PRIMARY KEY
) TABLESPACE sistemaInventarios;

COMMENT ON TABLE inventariosSchema.Publicidad IS 'Publicidad que pertenece al Glud';
COMMENT ON COLUMN inventariosSchema.Publicidad.k_idarticulo IS 'Llave primaria y foranea del articulo';

DROP TABLE IF EXISTS inventariosSchema.Otro  CASCADE ;

CREATE TABLE inventariosSchema.Otro (
  "k_idarticulo" int PRIMARY KEY
) TABLESPACE sistemaInventarios;

COMMENT ON TABLE inventariosSchema.Otro IS 'Otro tipo de articulos que le pertenecen al Glud';
COMMENT ON COLUMN inventariosSchema.Otro.k_idarticulo IS 'Llave primaria y foranea del articulo';

DROP TABLE IF EXISTS inventariosSchema.ArticuloInventario CASCADE ;

CREATE TABLE inventariosSchema.ArticuloInventario (
  "k_idinventario" int,
  "k_idarticulo" int,
  PRIMARY KEY ("k_idinventario", "k_idarticulo")
) TABLESPACE sistemaInventarios;

COMMENT ON TABLE inventariosSchema.ArticuloInventario IS 'Tabla de rompimiento entre un articulo y el inventario en donde esta registrado';
COMMENT ON COLUMN inventariosSchema.ArticuloInventario.k_idinventario IS 'Llave PFK, Id de la administracion de inventario';
COMMENT ON COLUMN inventariosSchema.ArticuloInventario.k_idarticulo IS 'Llave PFK, Id del articulo';

DROP TABLE IF EXISTS inventariosSchema.Prestamo CASCADE ;

CREATE TABLE inventariosSchema.Prestamo (
  "k_id" SERIAL PRIMARY KEY,
  "k_tipoidpersona" varchar(2) NOT NULL,
  "k_idpersona" int NOT NULL,
  "k_idinventario" int NOT NULL,
  "k_idarticulo" int NOT NULL,
  "d_fecha" date NOT NULL,
  "c_estado" varchar(16) NOT NULL,
  "c_observaciones" varchar(100)
) TABLESPACE	sistemaInventarios ;

COMMENT ON TABLE inventariosSchema.Prestamo IS 'Prestamos de los articulos que pertenecen al GLUD';
COMMENT ON COLUMN inventariosSchema.Prestamo.k_id IS 'Llave primaria del prestamo';
COMMENT ON COLUMN inventariosSchema.Prestamo.k_tipoidpersona IS 'Llave foranea, tipo de identificacion de la persona que solicita el prestamo';
COMMENT ON COLUMN inventariosSchema.Prestamo.k_idpersona IS 'Llave foranea, Identificacion de la persona que solicita el prestamo';
COMMENT ON COLUMN inventariosSchema.Prestamo.k_idinventario IS 'Llave foranea, Inventario en el cual se encuentra el articulo solicitado';
COMMENT ON COLUMN inventariosSchema.Prestamo.k_idarticulo IS 'Llave foranea, Articulo a prestar';
COMMENT ON COLUMN inventariosSchema.Prestamo.d_fecha IS 'Fecha del prestamo';
COMMENT ON COLUMN inventariosSchema.Prestamo.c_estado IS 'Estado del prestamo, varia entre activo, devuelto, inconcluso';
COMMENT ON COLUMN inventariosSchema.Prestamo.c_observaciones IS 'Observaciones del prestamo';

ALTER TABLE inventariosSchema.Prestamo ADD CONSTRAINT CHK_ESTADOPR CHECK (c_estado IN ('ACTIVO','DEVUELTO','INCONCLUSO'));

DROP TABLE IF EXISTS eventosSchema.Evento CASCADE ;

CREATE TABLE eventosSchema.Evento (
  "k_id" SERIAL PRIMARY KEY,
  "c_nombre" varchar(30) NOT NULL,
  "d_fecha" date,
  "k_idanioglud" int
) TABLESPACE	sistemaEventos ;

COMMENT ON TABLE eventosSchema.Evento IS 'Eventos realizados por el GLUD';
COMMENT ON COLUMN eventosSchema.Evento.k_id IS 'Llave primaria del evento';
COMMENT ON COLUMN eventosSchema.Evento.c_nombre IS 'Nombre del evento a realizar';
COMMENT ON COLUMN eventosSchema.Evento.d_fecha IS 'Fecha de realizacion';
COMMENT ON COLUMN eventosSchema.Evento.k_idanioglud IS 'Anio en que se realiza el registro';

DROP TABLE IF EXISTS eventosSchema.Actividad CASCADE ;

CREATE TABLE eventosSchema.Actividad (
  "k_id" SERIAL PRIMARY KEY,
  "c_nombre" varchar(30) NOT NULL,
  "d_fecha" date,
  "k_idevento" int NOT NULL,
  "c_ubicacion" varchar(20),
  "c_tipo" varchar(10),
  "c_duracion" varchar(10),
  "k_tipoidresponsable" varchar(2),
  "k_idresponsable" int,
  "c_organizacionresponsable" varchar(20)
) TABLESPACE sistemaEventos;

COMMENT ON TABLE eventosSchema.Actividad IS 'Actividades de cada Evento al interior del GLUD';
COMMENT ON COLUMN eventosSchema.Actividad.k_id IS 'Llave primaria de la actividad';
COMMENT ON COLUMN eventosSchema.Actividad.c_nombre IS 'Nombre de la actividad';
COMMENT ON COLUMN eventosSchema.Actividad.d_fecha IS 'Fecha de realizacion de la actividad';
COMMENT ON COLUMN eventosSchema.Actividad.k_idevento IS 'Llave foranea, Evento al cual pertenece la actividad';
COMMENT ON COLUMN eventosSchema.Actividad.c_ubicacion IS 'Ubicacion fisica o virtual en la cual se realiza la actividad';
COMMENT ON COLUMN eventosSchema.Actividad.c_tipo IS 'Tipo de actividad';
COMMENT ON COLUMN eventosSchema.Actividad.c_duracion IS 'Duracion en horas preferiblemente de la actividad';
COMMENT ON COLUMN eventosSchema.Actividad.k_tipoidresponsable IS 'Llave foranea, tipo de identificacion de la persona responsable  de la actividad';
COMMENT ON COLUMN eventosSchema.Actividad.k_idresponsable IS 'Llave foranea, identificacion de la persona responsable  de la actividad';
COMMENT ON COLUMN eventosSchema.Actividad.c_organizacionresponsable IS 'Organizacion a la que pertenece la persona responsable de la actividad';

ALTER TABLE eventosSchema.Actividad ADD CONSTRAINT CHK_TIPOA CHECK (c_tipo IN ('CONFERENCIA', 'TALLER', 'CURSO', 'COMPETENCIA', 'OTRO'));

DROP TABLE IF EXISTS eventosSchema.Patrocinador CASCADE ;

CREATE TABLE eventosSchema.Patrocinador (
  "k_tipoidpersona" varchar(2),
  "k_idpersona" int,
  "c_organizacion" varchar(20),
  PRIMARY KEY ("k_tipoidpersona", "k_idpersona")
) TABLESPACE sistemaEventos;

COMMENT ON TABLE eventosSchema.Patrocinador IS 'Patrocinadores que han ayudado en los eventos del Glud';
COMMENT ON COLUMN eventosSchema.Patrocinador.k_tipoidpersona IS 'Llave PFK del tipo de identificacion de la persona';
COMMENT ON COLUMN eventosSchema.Patrocinador.k_idpersona IS 'Llave PFK de la identificacion de la persona';
COMMENT ON COLUMN eventosSchema.Patrocinador.c_organizacion IS 'Nombre de la organizcion a la cual pertenece la persona que patrocina';

DROP TABLE IF EXISTS eventosSchema.Financiacion CASCADE ;

CREATE TABLE eventosSchema.Financiacion (
  "k_tipoidpatrocinador" varchar(2),
  "k_idpatrocinador" int,
  "k_idevento" int,
  "d_fecha" date,
  PRIMARY KEY ("k_tipoidpatrocinador", "k_idpatrocinador", "k_idevento")
) TABLESPACE sistemaEventos;

COMMENT ON TABLE eventosSchema.Financiacion IS 'Tabla de rompimiento de Financiacion de los eventos en el Glud';
COMMENT ON COLUMN eventosSchema.Financiacion.k_tipoidpatrocinador IS 'Llave PFK, tipo de Id del patrocinador';
COMMENT ON COLUMN eventosSchema.Financiacion.k_idpatrocinador IS 'Llave PFK, Id del patrocinador';
COMMENT ON COLUMN eventosSchema.Financiacion.k_idevento IS 'Llave PFK, Id del evento financiado';
COMMENT ON COLUMN eventosSchema.Financiacion.d_fecha IS 'Fecha en la cual se hace hace la financiacion';

DROP TABLE IF EXISTS eventosSchema.Inscripcion CASCADE ;

CREATE TABLE eventosSchema.Inscripcion (
  "k_tipoidpersona" varchar(2),
  "k_idpersona" int,
  "k_idactividad" int,
  "c_asiste" boolean,
  "c_gana" boolean,  
  "c_observaciones" varchar(50),
  PRIMARY KEY ("k_tipoidpersona", "k_idpersona", "k_idactividad")
) TABLESPACE sistemaEventos;

COMMENT ON TABLE eventosSchema.Inscripcion IS 'Tabla de rompimiento de Inscripcion de participantes a las actividades del Glud';
COMMENT ON COLUMN eventosSchema.Inscripcion.k_tipoidpersona IS 'Llave PFK, tipo de Id de la persona que se inscribe';
COMMENT ON COLUMN eventosSchema.Inscripcion.k_idpersona IS 'Llave PFK, Id de la persona que se inscribe';
COMMENT ON COLUMN eventosSchema.Inscripcion.k_idactividad IS 'Llave PFK, Id de la actividad a la cual se inscribe un participante';
COMMENT ON COLUMN eventosSchema.Inscripcion.c_asiste IS 'Valor booleano de asistencia de la persona inscrita a la actividad';
COMMENT ON COLUMN eventosSchema.Inscripcion.c_gana IS 'Valor booleano de victoria de la persona si la actividad es una competencia';
COMMENT ON COLUMN eventosSchema.Inscripcion.c_observaciones IS 'Observaciones adicionales de la inscripcion';

DROP TABLE IF EXISTS finanzasSchema.AdministracionTesoreria CASCADE ;

CREATE TABLE finanzasSchema.AdministracionTesoreria (
  "k_id" SERIAL PRIMARY KEY,
  "k_fechavigencia" int NOT NULL,
  "k_tipoidmiembro" varchar(2) NOT NULL,
  "k_idmiembro" int NOT NULL,
  "k_idcargo" int NOT NULL,
  "n_subtotalpresupuesto" int
) TABLESPACE sistemaFinanciero;

COMMENT ON TABLE finanzasSchema.AdministracionTesoreria IS 'Administracion de la Tesoreria al interior del Glud';
COMMENT ON COLUMN finanzasSchema.AdministracionTesoreria.k_id IS 'Llave primaria de la administracion de tesoreria';
COMMENT ON COLUMN finanzasSchema.AdministracionTesoreria.k_fechavigencia IS 'Llave foranea de la fecha de vigencia de la tesoreria';
COMMENT ON COLUMN finanzasSchema.AdministracionTesoreria.k_tipoidmiembro IS 'Llave foranea del tipo de identificacion del miembro a cargo de la tesoreria';
COMMENT ON COLUMN finanzasSchema.AdministracionTesoreria.k_idmiembro IS 'Llave foranea del numero de identificacion del miembro a cargo de la tesoreria';
COMMENT ON COLUMN finanzasSchema.AdministracionTesoreria.k_idcargo IS 'Llave foranea del cargo ejercido por el miembro a cargo de la tesoreria';
COMMENT ON COLUMN finanzasSchema.AdministracionTesoreria.n_subtotalpresupuesto IS 'Subtotal presupuesto';

ALTER TABLE finanzasSchema.AdministracionTesoreria ADD CONSTRAINT CHK_IDSUBTOTALPRESAT CHECK (n_subtotalpresupuesto>=0);

DROP TABLE IF EXISTS finanzasSchema.Movimiento CASCADE ;

CREATE TABLE finanzasSchema.Movimiento (
  "k_id" SERIAL PRIMARY KEY,
  "n_montomovimiento" int NOT NULL,
  "c_razonmovimiento" varchar(100) NOT NULL,
  "k_idadmintesoreria" int NOT NULL,
  "c_tipomovimiento" varchar(20) NOT NULL,
  "k_tipoidresponsable" varchar(2) NOT NULL,
  "k_idresponsable" int NOT NULL
) TABLESPACE sistemaFinanciero;

COMMENT ON TABLE finanzasSchema.Movimiento IS 'Movimientos financieros del Glud';
COMMENT ON COLUMN finanzasSchema.Movimiento.k_id IS 'Llave primaria del movimiento';
COMMENT ON COLUMN finanzasSchema.Movimiento.n_montomovimiento IS 'Valor por el cual se realiza el movimiento';
COMMENT ON COLUMN finanzasSchema.Movimiento.c_razonmovimiento IS 'Justificacion motivo por el cual se hace el movimiento';
COMMENT ON COLUMN finanzasSchema.Movimiento.k_idadmintesoreria IS 'Llave foranea que referencia la administracion de la tesoreria en la cual se hace el movimiento';
COMMENT ON COLUMN finanzasSchema.Movimiento.c_tipomovimiento IS 'Tipo de movimiento, puede ser ingreso, inversion, perdida';
COMMENT ON COLUMN finanzasSchema.Movimiento.k_tipoidresponsable IS 'Llave foranea del tipo de identificacion de la persona responsable del movimiento';
COMMENT ON COLUMN finanzasSchema.Movimiento.k_idresponsable IS 'Llave foranea de la identificacion de la persona responsable del movimiento';

ALTER TABLE finanzasSchema.Movimiento ADD CONSTRAINT CHK_MONTOM CHECK (n_montomovimiento>0);
ALTER TABLE finanzasSchema.Movimiento ADD CONSTRAINT CHK_TIPOM CHECK (c_tipomovimiento IN ('INGRESO', 'EGRESO'));
