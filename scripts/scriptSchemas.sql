/*  Grupo GNU Linux UD - 2020 
    LeidyMarcelaAldana@gmail.com
    chayderarmando@gmail.com
*/

/*SECOND SCRIPT!!!*/

/*
Create Schemas 

For switch between Schemas use:
SET search_path TO nameSchema;
*/
CREATE SCHEMA IF NOT EXISTS generalSchema AUTHORIZATION DBAdmin;
CREATE SCHEMA IF NOT EXISTS eventosSchema AUTHORIZATION DBAdmin;
CREATE SCHEMA IF NOT EXISTS inventariosSchema AUTHORIZATION DBAdmin;
CREATE SCHEMA IF NOT EXISTS finanzasSchema AUTHORIZATION DBAdmin;
CREATE SCHEMA IF NOT EXISTS proyectosSchema AUTHORIZATION DBAdmin;

COMMENT ON SCHEMA generalSchema IS 'Esquema logico para el sistema general';
COMMENT ON SCHEMA eventosSchema IS 'Esquema logico para el sistema de eventos';
COMMENT ON SCHEMA inventariosSchema IS 'Esquema logico para el sistema de inventarios';
COMMENT ON SCHEMA finanzasSchema IS 'Esquema logico para el sistema financiero';
COMMENT ON SCHEMA proyectosSchema IS 'Esquema logico para el sistema de proyectos';


