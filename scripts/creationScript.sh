#!/bin/bash

#    Grupo GNU Linux UD - 2020 
#    LeidyMarcelaAldana@gmail.com
#    chayderarmando@gmail.com

#docker run --name postgresServer -e POSTGRES_PASSWORD=[yourPassHere] -d -p [yourPortHere]:5432 -v [yourScriptFolderLocationHere]:/scripts postgres
#docker exec -it postgresServer /bin/bash
#sh /scripts/creationScript.sh

mkdir -p /data/DBGLUD
cd /data/DBGLUD
mkdir general
mkdir eventos
mkdir inventarios
mkdir finanzas
mkdir proyectos

chown -R postgres:postgres /data

psql -U postgres -d postgres -a -f /scripts/scriptTableSpacesRolsAndDB.sql
psql -U dbadmin -d db_glud -a -f /scripts/scriptSchemas.sql
psql -U dbadmin -d db_glud -a -f /scripts/scriptCreateTables.sql
psql -U dbadmin -d db_glud -a -f /scripts/scriptForeignKeys.sql
psql -U dbadmin -d db_glud -a -f /scripts/scriptPrivileges.sql
