/*  Grupo GNU Linux UD - 2020 
    LeidyMarcelaAldana@gmail.com
    chayderarmando@gmail.com
*/

/*FOURTH SCRIPT*/

--Foreing Keys
SET search_path TO generalSchema, eventosSchema, inventariosSchema, finanzasSchema, proyectosSchema, "$user", public;

ALTER TABLE Miembro ADD CONSTRAINT FK_MIEMBROPERSONA FOREIGN KEY ("k_tipoid", "k_id") REFERENCES Persona ("k_tipoid", "k_id");

ALTER TABLE Miembro ADD CONSTRAINT FK_MIEMBRODOCENTE FOREIGN KEY ("k_tipoiddocente", "k_iddocente") REFERENCES Miembro ("k_tipoid", "k_id");

ALTER TABLE Miembro ADD CONSTRAINT FK_MIEMBROANIOGLUD FOREIGN KEY ("k_idanioglud") REFERENCES AnioGlud ("k_id");

ALTER TABLE Miembro ADD CONSTRAINT FK_MIEMBROPCURRICULAR FOREIGN KEY ("k_idproyectocurricular") REFERENCES ProyectoCurricular ("k_id");

ALTER TABLE CargoEjercido ADD CONSTRAINT FK_CARGOEJMIEMBRO FOREIGN KEY ("k_tipoidmiembro", "k_idmiembro") REFERENCES Miembro ("k_tipoid", "k_id");

ALTER TABLE CargoEjercido ADD CONSTRAINT FK_CARGOEJCARGO FOREIGN KEY ("k_idcargo") REFERENCES Cargo ("k_id");

ALTER TABLE MiembroProyecto ADD CONSTRAINT FK_MPROYECTOMIEMBRO FOREIGN KEY ("k_tipoidmiembro", "k_idmiembro") REFERENCES Miembro ("k_tipoid", "k_id");

ALTER TABLE MiembroProyecto ADD CONSTRAINT FK_MPROYECTOPROYECTO FOREIGN KEY ("k_idproyecto") REFERENCES Proyecto ("k_id");

ALTER TABLE AdministracionInventario ADD CONSTRAINT FK_ADMININVANIOGLUD FOREIGN KEY ("k_fechavigencia") REFERENCES AnioGlud ("k_id");

ALTER TABLE AdministracionInventario ADD CONSTRAINT FK_ADMININVCARGOEJERCIDO FOREIGN KEY ("k_tipoidmiembro", "k_idmiembro", "k_idcargo") REFERENCES CargoEjercido ("k_tipoidmiembro", "k_idmiembro", "k_idcargo");

ALTER TABLE Libro ADD CONSTRAINT FK_LIBROARTICULO FOREIGN KEY ("k_idarticulo") REFERENCES Articulo ("k_id");

ALTER TABLE ArticuloElectronico ADD CONSTRAINT FK_ARTELECTARTICULO FOREIGN KEY ("k_idarticulo") REFERENCES Articulo ("k_id");

ALTER TABLE Miscelanea ADD CONSTRAINT FK_MISCELANEAARTICULO FOREIGN KEY ("k_idarticulo") REFERENCES Articulo ("k_id");

ALTER TABLE Juego ADD CONSTRAINT FK_JUEGOARTICULO FOREIGN KEY ("k_idarticulo") REFERENCES Articulo ("k_id");

ALTER TABLE Publicidad ADD CONSTRAINT FK_PUBLICIDADARTICULO FOREIGN KEY ("k_idarticulo") REFERENCES Articulo ("k_id");

ALTER TABLE Otro ADD CONSTRAINT FK_OTROARTICULO FOREIGN KEY ("k_idarticulo") REFERENCES Articulo ("k_id");

ALTER TABLE ArticuloInventario ADD CONSTRAINT FK_ARTINVADMININV FOREIGN KEY ("k_idinventario") REFERENCES AdministracionInventario ("k_id");

ALTER TABLE ArticuloInventario ADD CONSTRAINT FK_ARTINVARTICULO FOREIGN KEY ("k_idarticulo") REFERENCES Articulo ("k_id");

ALTER TABLE Prestamo ADD CONSTRAINT FK_PRESTAMOPERSONA FOREIGN KEY ("k_tipoidpersona", "k_idpersona") REFERENCES Persona ("k_tipoid", "k_id");

ALTER TABLE Prestamo ADD CONSTRAINT FK_PRESTAMOARTINVENTARIO FOREIGN KEY ("k_idinventario", "k_idarticulo") REFERENCES ArticuloInventario ("k_idinventario", "k_idarticulo");

ALTER TABLE Evento ADD CONSTRAINT FK_EVENTOANIOGLUD FOREIGN KEY ("k_idanioglud") REFERENCES AnioGlud ("k_id");

ALTER TABLE Actividad ADD CONSTRAINT FK_ACTIVIDADEVENTO FOREIGN KEY ("k_idevento") REFERENCES Evento ("k_id");

ALTER TABLE Actividad ADD CONSTRAINT FK_ACTIVIDADRESPONSABLE FOREIGN KEY ("k_tipoidresponsable", "k_idresponsable") REFERENCES Persona ("k_tipoid", "k_id");

ALTER TABLE Patrocinador ADD CONSTRAINT FK_PATROCINADORPERSONA FOREIGN KEY ("k_tipoidpersona", "k_idpersona") REFERENCES Persona ("k_tipoid", "k_id");

ALTER TABLE Financiacion ADD CONSTRAINT FK_FINANCIACIONPATROCINADOR FOREIGN KEY ("k_tipoidpatrocinador", "k_idpatrocinador") REFERENCES Patrocinador ("k_tipoidpersona", "k_idpersona");

ALTER TABLE Financiacion ADD CONSTRAINT FK_FINANCIACIONEVENTO FOREIGN KEY ("k_idevento") REFERENCES Evento ("k_id");

ALTER TABLE Inscripcion ADD CONSTRAINT FK_INSCRIPCIONPERSONA FOREIGN KEY ("k_tipoidpersona", "k_idpersona") REFERENCES Persona ("k_tipoid", "k_id");

ALTER TABLE Inscripcion ADD CONSTRAINT FK_INCRIPCIONACTIVIDAD FOREIGN KEY ("k_idactividad") REFERENCES Actividad ("k_id");

ALTER TABLE AdministracionTesoreria ADD CONSTRAINT FK_ADMINTESANIOGLUD FOREIGN KEY ("k_fechavigencia") REFERENCES AnioGlud ("k_id");

ALTER TABLE AdministracionTesoreria ADD CONSTRAINT FK_ADMINTESCARGOEJERCIDO FOREIGN KEY ("k_tipoidmiembro", "k_idmiembro", "k_idcargo") REFERENCES CargoEjercido ("k_tipoidmiembro", "k_idmiembro", "k_idcargo");

ALTER TABLE Movimiento ADD CONSTRAINT FK_MOVIMIENTOADMINTESORERIA FOREIGN KEY ("k_idadmintesoreria") REFERENCES AdministracionTesoreria ("k_id");

ALTER TABLE Movimiento ADD CONSTRAINT FK_MOVIMIENTORESPONSABLE FOREIGN KEY ("k_tipoidresponsable", "k_idresponsable") REFERENCES Persona ("k_tipoid", "k_id");