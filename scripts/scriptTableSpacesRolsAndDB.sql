/*  Grupo GNU Linux UD - 2020 
    LeidyMarcelaAldana@gmail.com
    chayderarmando@gmail.com
*/

/*FIRST SCRIPT!!!*/

/*
Rol's creation for DBGLUD
*/
CREATE ROLE DBAdmin WITH SUPERUSER LOGIN PASSWORD '[unaContraseña]';
CREATE ROLE Director WITH LOGIN PASSWORD '[dosContraseñas]' NOINHERIT;
CREATE ROLE Tesorero WITH LOGIN PASSWORD '[tresContraseñas]' NOINHERIT;
CREATE ROLE AdminSala WITH LOGIN PASSWORD '[cuatroContraseñas]' NOINHERIT;
CREATE ROLE Miembro WITH LOGIN PASSWORD '[cincoContraseñas]' NOINHERIT;
CREATE ROLE Invitado WITH LOGIN PASSWORD '[seisContraseñas]' NOINHERIT;

COMMENT ON ROLE DBAdmin IS 'Rol para el administrador de la base de datos';
COMMENT ON ROLE Director IS 'Rol para el director del GLUD';
COMMENT ON ROLE Tesorero IS 'Rol para el tesorero del GLUD';
COMMENT ON ROLE AdminSala IS 'Rol para el adminsala del GLUD';
COMMENT ON ROLE Miembro IS 'Rol para los miembros del GLUD';
COMMENT ON ROLE Invitado IS 'Rol para los invitados externos del GLUD';
/*
Create DBA User

CREATE USER DBAdmin WITH PASSWORD 'th1sn0tmyp@ss';
GRANT DBA to DBAdmin;
*/
/*
Tablespace's creation with DBA
*/
CREATE TABLESPACE sistemaGeneral OWNER DBAdmin LOCATION '/data/DBGLUD/general';
CREATE TABLESPACE sistemaEventos OWNER DBAdmin LOCATION '/data/DBGLUD/eventos';
CREATE TABLESPACE sistemaInventarios OWNER DBAdmin LOCATION '/data/DBGLUD/inventarios';
CREATE TABLESPACE sistemaFinanciero OWNER DBAdmin LOCATION '/data/DBGLUD/finanzas';
CREATE TABLESPACE sistemaProyectos OWNER DBAdmin LOCATION '/data/DBGLUD/proyectos';

COMMENT ON TABLESPACE sistemaGeneral IS 'Tablespace para los objetos del sistema general';
COMMENT ON TABLESPACE sistemaEventos IS 'Tablespace para los objetos del sistema de eventos';
COMMENT ON TABLESPACE sistemaInventarios IS 'Tablespace para los objetos del sistema de inventarios';
COMMENT ON TABLESPACE sistemaFinanciero IS 'Tablespace para los objetos del sistema financiero';
COMMENT ON TABLESPACE sistemaProyectos IS 'Tablespaces para los objetos del sistema de proyectos';

/*
CREATE DATABASE
*/
CREATE DATABASE db_glud OWNER DBAdmin TABLESPACE sistemaGeneral;
COMMENT ON DATABASE db_glud IS 'Base de datos del grupo GNU/Linux UD';